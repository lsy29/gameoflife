const unitLength = 20;
const boxColor = 150;
const strokeColor = 50;
let columns; /* To be determined by window width*/
let rows; /* To be determined by window height */
let currentBoard;
let nextBoard;
// let age = 0;
let boardBGColor;
let slider;
// let  sliderFR = 30;
let clr;
let rectX;
let dead = "#28527a"; // darkBlue
// let trail;
let alive = "#8ac4d0"; //lightBlue
let deadR;
let aliveR;
let aRGB;
let dRGB;
let defaultColor = true;
let darkenColor = false;
let lightenColor = false;
let randomColor = false;
let freezeGame = false;
let startGame = true;
let once = true;
let uInrules1;
let uInrules;




// function sliderF(){

//     // slider
//     slider = createSlider(0, 255, 30);
//     slider.position(230, 800);
//     slider.style('width', '80px');

//     console.log("slider: " + slider);
//     return slider;
// }

function setup() {
    /* Set the canvas to be under the element #canvas*/
    const canvas = createCanvas(windowWidth - 200, windowHeight - 120 + 20);
    canvas.parent(document.querySelector('#canvas'));

    // slider


    /*Calculate the number of columns and rows */
    columns = floor(width / unitLength);
    rows = floor(height / unitLength);

    /*Making both currentBoard and nextBoard 2-dimensional matrix that has (columns * rows) boxes. */
    currentBoard = [];
    nextBoard = [];
    // age = 0;
    for (let i = 0; i < columns; i++) {
        currentBoard[i] = [];
        nextBoard[i] = [];
        // age[i] = [];
    }
    // Now both currentBoard and nextBoard are array of array of undefined values.
    init(); // Set the initial values of the currentBoard and nextBoard
}


/**
 * Initialize/reset the board state
 */
function init() {
    for (let i = 0; i < columns; i++) {
        for (let j = 0; j < rows; j++) {
            currentBoard[i][j] = 0;
            nextBoard[i][j] = 0;
            // age[i][j] = 0;
        }
    }
}

function randomF() {
    for (let i = 0; i < columns; i++) {
        for (let j = 0; j < rows; j++) {
            currentBoard[i][j] = random() > 0.8 ? 1 : 0;
            nextBoard[i][j] = 0;
            // age[i][j] = 0;
        }
    }
}


// Simple example, see optional options for more configuration.
const pickr = Pickr.create({
    el: '.color-picker',
    theme: 'classic', // or 'monolith', or 'nano'
    default: '#726a95',

    swatches: [
        '#333333',
        '#000000'
    ],

    components: {

        // Main components
        preview: true,
        opacity: true,
        hue: true,

        // Input / output Options
        interaction: {
            hex: true,
            rgba: true,
            Input: true
        }
    }
});

boardBGColor = pickr.options.default
pickr.on('change', (color, instance) => {
    const rgb = color.toHEXA();
    const rgbaColor = "#" + rgb[0] + rgb[1] + rgb[2]; // get number from array
    console.log({ rgbaColor });
    boardBGColor = rgbaColor;
});



// // let r = 123;
// // console.log(r.toString(16));   // transfer number to 16 進制

console.log("boardBGColor: " + boardBGColor);


// function defaultColor() {
//     dead = "#28527a" // darkBlue
//     alive = "#8ac4d0" //lightBlue
// }

function colorStyleDarken() {
    dead = "#000000" // black
    alive = "#808080" //gray
}

function colorStyleLighten() {
    dead = "#fa1e0e" // red
    alive = "#ffbe0f" //yellow
    console.log("lighten color: dead: " + dead + ", alive: " + alive);
}

function colorStyleRandom() {

    function deadF() {
        const dR = Math.floor(Math.random() * 255);
        const dG = Math.floor(Math.random() * 255);
        const dB = Math.floor(Math.random() * 255);
        deadR = "#" + dR.toString(16) + dG.toString(16) + dB.toString(16);
        console.log("random deadR color: " + deadR);
        // fill(boardBGColor);
    }

    function aliveF() {
        const aR = Math.floor(Math.random() * 255);
        const aG = Math.floor(Math.random() * 255);
        const aB = Math.floor(Math.random() * 255);
        aliveR = "#" + aR.toString(16) + aG.toString(16) + aB.toString(16);
        console.log("random aliveR color: " + aliveR);
        // fill(boardBGColor);
    }

    deadF();
    aliveF();

}


function colorStyleBtnTrigger() {
    var colorDarkenBtn = document.querySelector('#darkenBtn')
    colorDarkenBtn.addEventListener('click', function() {
        colorStyleDarken()
        console.log("DarkenBtn pressed");
        lightenColor = false;
        randomColor = false;
        return darkenColor = true;

    })

    var colorLightenBtn = document.querySelector('#lightenBtn')
    colorLightenBtn.addEventListener('click', function() {
        colorStyleLighten()
        console.log("LightenBtn pressed");
        darkenColor = false;
        randomColor = false;
        return lightenColor = true;
    })

    var colorRandomBtn = document.querySelector('#randomBtn')
    colorRandomBtn.addEventListener('click', function() {
        colorStyleRandom()
        dRGB = deadR;
        aRGB = aliveR;
        console.log("RandomBtn pressed");
        darkenColor = false;
        lightenColor = false;
        return randomColor = true;
    })
}

colorStyleBtnTrigger();


function draw() {
    background(255);
    // rectX = rectX += 1;
    generate();
    // dead = "#fa1e0e";   // red
    // alive = "#ffbe0f";  // yellow
    // alive = "#28527a";  // yellow
    // trail = "#28527a";  // blue // never execute
    // trail = "#ffbe0f";  // blue // never execute

    // console.log("#age" + age);

    for (let i = 0; i < columns; i++) {

        for (let j = 0; j < rows; j++) {
            if (currentBoard[i][j] == 1) {

                // fill("#98ded9"); 

                // if (nextBoard[i][çj] == 0) {
                //     fill(dead);
                // } else if (nextBoard[i][j] == 1) {
                //     fill(alive);
                // }

                if (darkenColor) {
                    colorStyleDarken();

                    if (nextBoard[i][j] == 0) {
                        fill(dead);
                    } else if (nextBoard[i][j] == 1) {
                        fill(alive);
                    }

                    // return darkenColor = false;

                } else if (lightenColor) {
                    colorStyleLighten();

                    if (nextBoard[i][j] == 0) {
                        fill(dead);
                    } else if (nextBoard[i][j] == 1) {
                        fill(alive);
                    }

                    // return lightenColor = false;

                } else if (randomColor) {
                    colorStyleRandom();

                    if (nextBoard[i][j] == 0) {
                        fill(dRGB);
                    } else if (nextBoard[i][j] == 1) {
                        fill(aRGB);
                    }

                    // return randomColor = false;

                } else if (defaultColor && !darkenColor && !lightenColor && !randomColor) {

                    if (nextBoard[i][j] == 0) {
                        fill(dead);
                    } else if (nextBoard[i][j] == 1) {
                        fill(alive);
                    }

                    console.log("nextBoard[i][j]: " + nextBoard[i][j]);

                }


            } else {

                // fill("IndianRed");
                fill(boardBGColor);

            }
            stroke(strokeColor);
            rect(i * unitLength, j * unitLength, unitLength, unitLength, 20);
            // ellipse(i * 14,j * 14, 14, 14);

            //polygon(i, j, unitLength, 20);

        }
    }

    // let val = slider.value();
    // // background(val);

}

//frameRate(slider.value());

function generate() {
    //Loop over every single box on the board
    for (let x = 0; x < columns; x++) {
        for (let y = 0; y < rows; y++) {
            // Count all living members in the Moore neighborhood(8 boxes surrounding)
            let neighbors = 0;
            for (let i of[-1, 0, 1]) {
                for (let j of[-1, 0, 1]) {
                    if (i === 0 && j === 0) {
                        // the cell itself is not its own neighbor
                        continue;
                    }
                    // The modulo operator is crucial for wrapping on the edge
                    neighbors += currentBoard[(x + i + columns) % columns][(y + j + rows) % rows];
                }
            }

            // Rules of Life
            if (currentBoard[x][y] == 1 && neighbors < 2) {
                // Die of Loneliness
                nextBoard[x][y] = 0;
                // age = 0;
                // return age;
            } else if (currentBoard[x][y] == 1 && neighbors > 3) {
                // Die of Overpopulation
                nextBoard[x][y] = 0;
                // age = 0;
                // return age;
            } else if (currentBoard[x][y] == 0 && neighbors == 3) {
                // New life due to Reproduction
                nextBoard[x][y] = 1;
                // age++;
                // return age;
            } else {
                // Stasis
                nextBoard[x][y] = currentBoard[x][y];
                // age = 0;
                // return age;
            }

        }

        // console.log("age: " + age);
    }

    // Swap the nextBoard to be the current Board
    [currentBoard, nextBoard] = [nextBoard, currentBoard];
}



/**
 * When mouse is dragged
 */
function mouseDragged() {
    /**
     * If the mouse coordinate is outside the board
     */

    if (!freezeGame && startGame) {

        if (mouseX > unitLength * columns || mouseY > unitLength * rows) {
            return;
        }
        const x = Math.floor(mouseX / unitLength);
        const y = Math.floor(mouseY / unitLength);
        currentBoard[x][y] = 1;
        fill(boxColor);
        stroke(strokeColor);
        rect(x * unitLength, y * unitLength, unitLength, unitLength, 20);
        // ellipse(x * 25,y * 25, 14, 14);
        // polygon((x * unitLength) /2, (y * unitLength) /2, 5 , 100);
        // polygon(0, 0, 80, 7);

    } else if (freezeGame && !startGame) {
        noLoop();

    }



}

/**
 * When mouse is pressed
 */
function mousePressed() {
    noLoop();
    mouseDragged();
}

/**
 * When mouse is released
 */
function mouseReleased() {

    if (!freezeGame && startGame) {
        loop();
    } else if (freezeGame && !startGame) {
        noLoop();
    }

}


let startBtn = document.querySelector('#start-game');
startBtn.addEventListener('click', function() {
    return startGame = true;
})

function btnStart() {
    loop();
    return freezeGame = false;
}

let freezeBtn = document.querySelector('#freeze-game')
freezeBtn.addEventListener('click', function() {
    return freezeGame = true;
})

function btnFreeze() {
    noLoop();
    return startGame = true;

}


function btnRandom() {
    randomF();
}

let resetBtn = document.querySelector('.reset-game')
resetBtn.addEventListener('click', function() {
    init();
});

let sliderBar = document.querySelector('#frSlider')
sliderBar.addEventListener('change', function() {
    frameRate(parseInt(sliderBar.value));
});




function test() {
    console.log('pickr = ', pickr)
    const rgb = pickr.options.default;
    //const rgbaColor = "#" + rgb[0] + rgb[1] + rgb[2];  // get number from array
    console.log(rgb);
    boardBGColor = rgb;
}